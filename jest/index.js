const { packageJson, install } = require("mrm-core");
const { sortPackageJson } = require("../utils");

module.exports = function task() {
	install(["jest", "@html-validate/jest-config"]);

	const pkg = packageJson();
	pkg.setScript("test", "jest");
	pkg.set("jest", {
		preset: "@html-validate/jest-config",
	});
	pkg.save();
	sortPackageJson();

	const hasTypescript = pkg.get("devDependencies.typescript", false);
	if (hasTypescript) {
		install("@types/jest");
	}
};

module.exports.description = "Add jest support";
