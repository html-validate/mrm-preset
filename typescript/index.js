const { packageJson, install, json, lines } = require("mrm-core");
const semver = require("semver");
const { sortPackageJson } = require("../utils");

module.exports = function task() {
	const pkg = packageJson();
	const nodeRange = pkg.get("engines.node", ">= 12");
	const nodeMajor = semver.parse(semver.minVersion(nodeRange)).major;

	install("typescript", `@types/node@${nodeMajor}`);

	pkg.setScript("build", "tsc");
	pkg.save();
	sortPackageJson();

	const hasPrettier = pkg.get("devDependencies.prettier", false);

	// .tsconfig
	json("tsconfig.json")
		.merge({
			compilerOptions: {
				alwaysStrict: true,
				baseUrl: "./src",
				declaration: true,
				esModuleInterop: true,
				lib: ["es2019"],
				module: "commonjs",
				noImplicitAny: true,
				outDir: "./dist",
				sourceMap: true,
				target: "es2019",
			},
			include: ["src/**/*.ts"],
			exclude: ["node_modules"],
		})
		.save();
	if (hasPrettier) {
		prettier("tsconfig.json");
	}

	// .gitignore
	lines(".gitignore").add(["dist/"]).save();

	// .prettierignore
	if (hasPrettier) {
		lines(".prettierignore").add(["dist/"]).save();
	}
};

module.exports.description = "Add typescript support";
