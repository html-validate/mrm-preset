const { packageJson, uninstall, yaml } = require("mrm-core");
const { sortPackageJson, prettier } = require("../utils");

module.exports = function task() {
	const pkg = packageJson();
	pkg.removeScript("semantic-release");

	const preset = pkg.set("release.extends", false);
	if (!preset) {
		pkg.set("release", {
			extends: "@html-validate/semantic-release-config",
		});
	}

	pkg.save();
	sortPackageJson();

	uninstall([
		"semantic-release",
		"@semantic-release/changelog",
		"@semantic-release/exec",
		"@semantic-release/git",
		"@semantic-release/gitlab",
		"@semantic-release/npm",
		"@semantic-release/release-notes-generator",
		"@html-validate/semantic-release-config",
		"@html-validate/semantic-release-monorepo-config",
	]);

	const file = yaml(".gitlab-ci.yml");
	if (file.exists()) {
		file.set([".release"], {
			stage: "release",
			variables: {
				GIT_AUTHOR_NAME: "${GITLAB_USER_NAME}",
				GIT_AUTHOR_EMAIL: "${GITLAB_USER_EMAIL}",
				GIT_COMMITTER_NAME: "${HTML_VALIDATE_BOT_NAME}",
				GIT_COMMITTER_EMAIL: "${HTML_VALIDATE_BOT_EMAIL}",
			},
			/* eslint-disable-next-line camelcase -- output should be in camelcase */
			before_script: [
				"export PRESET=$(node -p 'require(\"./package.json\").release.extends')",
				"echo Using preset ${PRESET}",
				"npm install --no-save ${PRESET}",
			],
		});

		file.set(["Dry run"], {
			extends: ".release",
			needs: [],
			dependencies: [],
			rules: [
				{ if: '$CI_COMMIT_REF_NAME == "master"' },
				{ if: "$CI_COMMIT_REF_NAME =~ /^release\\//" },
			],
			script: ["npm exec semantic-release -- --dry-run"],
		});

		file.set(["Release"], {
			extends: ".release",
			rules: [
				{
					if: '$CI_COMMIT_REF_NAME == "master" && $CI_PIPELINE_SOURCE == "schedule"',
					when: "on_success",
				},
				{
					if: '$CI_COMMIT_REF_NAME == "master" && $CI_PIPELINE_SOURCE == "web"',
					when: "manual",
				},
				{
					if: '$CI_COMMIT_REF_NAME =~ /^release\\// && $CI_PIPELINE_SOURCE == "web"',
					when: "manual",
				},
			],
			script: ["npm exec semantic-release"],
		});

		file.save();
		prettier(".gitlab-ci.yml");
	}
};

module.exports.description = "Configure semantic-release support";
