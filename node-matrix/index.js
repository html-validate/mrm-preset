const { yaml } = require("mrm-core");
const { prettier } = require("../utils");

module.exports = function task() {
	const file = yaml(".gitlab-ci.yml");
	if (!file.exists()) {
		return;
	}

	for (const major of [10, 11, 12, 13, 14, 15, 16]) {
		for (const tag of ["LTS", "current"]) {
			file.unset(`Node ${major}.x (${tag})`);
		}
	}

	file.set("Node", {
		stage: "compatibility",
		parallel: {
			matrix: [{ VERSION: [12, 14, 16] }],
		},
		dependencies: ["NPM", "Build"],
		needs: ["NPM", "Build"],
		image: "node:${VERSION}",
		script: ["npm test -- --no-coverage"],
	});

	file.save();
	prettier(".gitlab-ci.yml");
};

module.exports.description = "Use matrix for testing node versions compatibilty";
