/* eslint-disable sonarjs/no-os-command-from-path -- expected to lookup from PATH */

const { spawnSync } = require("child_process");
const { packageJson, install, deleteFiles } = require("mrm-core");

/**
 * Setup husky for project.
 * @returns {void}
 */
function huskySetup() {
	install(["husky"]);
	const pkg = packageJson();
	pkg.setScript("prepare", "husky install");
	pkg.save();
	deleteFiles([".husky/.gitignore"]);
}

/**
 * Add hook to husky.
 *
 * @param {string} hook - Hook to add
 * @param {string | string[]} script - Script to run
 * @returns {void}
 */
function huskySet(hook, script) {
	if (Array.isArray(script)) {
		script = script.join("\n");
	}
	install("husky");
	spawnSync("npm", ["exec", "husky", "install"], {
		encoding: "utf-8",
	});
	spawnSync("npm", ["exec", "husky", "set", `.husky/${hook}`, script], {
		encoding: "utf-8",
	});
}

module.exports = {
	huskySetup,
	huskySet,
};
