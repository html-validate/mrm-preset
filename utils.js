/* eslint-disable sonarjs/no-os-command-from-path -- expected to lookup from PATH */

const { spawnSync } = require("child_process");

/**
 * @param {string} filename
 * @returns {void}
 */
function prettier(filename) {
	spawnSync("npx", ["prettier", "--write", filename]);
}

/**
 * @returns {void}
 */
function sortPackageJson() {
	spawnSync("npx", ["sort-package-json"]);
}

module.exports = {
	prettier,
	sortPackageJson,
};
