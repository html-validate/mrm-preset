const { packageJson } = require("mrm-core");
const { sortPackageJson } = require("../utils");

module.exports = function task() {
	const pkg = packageJson();
	pkg.set("renovate", {
		extends: ["gitlab>html-validate/renovate-config"],
	});
	pkg.save();
	sortPackageJson();
};

module.exports.description = "Enable renovate with html-validate config";
