const { packageJson, install, uninstall } = require("mrm-core");
const { huskySetup, huskySet } = require("../husky-utils");
const { sortPackageJson } = require("../utils");

module.exports = function task() {
	install(["@html-validate/commitlint-config"]);
	uninstall(["@commitlint/cli", "@commitlint/config-conventional", "commitlint"]);

	huskySetup();
	huskySet("commit-msg", 'exec npm exec commitlint -- --edit "$1"');

	const pkg = packageJson();
	pkg.set("commitlint", {
		extends: "@html-validate",
	});
	pkg.save();
	sortPackageJson();
};

module.exports.description = "Add commitlint";
