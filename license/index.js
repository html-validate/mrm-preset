const path = require("path");
const { file, packageJson, template } = require("mrm-core");

function applyMit() {
	template("LICENSE", path.join(__dirname, "LICENSE"))
		.apply({
			year: new Date().getFullYear(),
			name: "David Sveningsson",
			email: "ext@sidvind.com",
		})
		.save();
}

module.exports = function task() {
	const pkg = packageJson();
	const license = pkg.get("license");

	switch (license) {
		case "MIT":
			applyMit();
			break;
		default:
			throw new Error(`Unknown license "${license}"`);
	}

	const old = file("LICENCE");
	old.delete();
};

module.exports.description = "Manage license";
