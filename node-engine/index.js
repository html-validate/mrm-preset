/* eslint-disable sonarjs/no-os-command-from-path -- expected to lookup from PATH */

const { spawnSync } = require("child_process");
const { packageJson, yaml } = require("mrm-core");

function updatePackageJson() {
	const pkg = packageJson();
	pkg.merge({
		engines: {
			node: ">= 12.0",
		},
	});
	pkg.save();
	spawnSync("npm", ["install"]);
	spawnSync("npx", ["sort-package-json"]);
}

function updateGitlabCI() {
	const file = yaml(".gitlab-ci.yml");
	if (!file.exists()) {
		return;
	}

	if (file.get("Node")) {
		file.set("Node.parallel.matrix[0].VERSION", [12, 14, 16]);
	}

	file.unset("Node 10.x (LTS)");

	file.save();
	spawnSync("npx", ["prettier", "--write", ".gitlab-ci.yml"]);
}

module.exports = function task() {
	updatePackageJson();
	updateGitlabCI();
};

module.exports.description = "Update node engine";
