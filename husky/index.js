const { huskySetup } = require("../husky-utils");

module.exports = function task() {
	huskySetup();
};

module.exports.description = "Add husky";
